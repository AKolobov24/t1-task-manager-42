package ru.t1.akolobov.tm.exception.field;

public final class DateEmptyException extends AbstractFieldException {

    public DateEmptyException() {
        super("Error! Date is empty...");
    }

}
