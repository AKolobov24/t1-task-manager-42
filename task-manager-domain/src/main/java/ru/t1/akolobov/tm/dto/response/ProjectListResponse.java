package ru.t1.akolobov.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.t1.akolobov.tm.dto.model.ProjectDTO;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class ProjectListResponse extends AbstractResponse {

    @NotNull
    private List<ProjectDTO> projectList = new ArrayList<>();

    public ProjectListResponse(@NotNull List<ProjectDTO> projectList) {
        this.projectList = projectList;
    }

}
