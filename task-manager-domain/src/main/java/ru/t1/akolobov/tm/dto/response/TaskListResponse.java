package ru.t1.akolobov.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.t1.akolobov.tm.dto.model.TaskDTO;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class TaskListResponse extends AbstractResponse {

    @NotNull
    private List<TaskDTO> taskList = new ArrayList<>();

    public TaskListResponse(@NotNull List<TaskDTO> taskList) {
        this.taskList = taskList;
    }

}
