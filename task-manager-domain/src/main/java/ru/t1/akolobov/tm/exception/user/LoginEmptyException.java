package ru.t1.akolobov.tm.exception.user;

public final class LoginEmptyException extends AbstractUserException {

    public LoginEmptyException() {
        super("Error! Login is empty...");
    }

}
