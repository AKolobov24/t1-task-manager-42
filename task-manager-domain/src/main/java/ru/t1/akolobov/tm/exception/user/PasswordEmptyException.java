package ru.t1.akolobov.tm.exception.user;

public final class PasswordEmptyException extends AbstractUserException {

    public PasswordEmptyException() {
        super("Error! Password is empty...");
    }

}
