package ru.t1.akolobov.tm.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.akolobov.tm.exception.system.InputNotSupportedException;

import java.util.Optional;
import java.util.Scanner;

public interface TerminalUtil {

    @NotNull
    Scanner SCANNER = new Scanner(System.in);

    @Nullable
    static String nextLine() {
        return SCANNER.nextLine();
    }

    @NotNull
    static Integer nextNumber() {
        return Integer.parseInt(
                Optional.ofNullable(nextLine())
                        .orElseThrow(InputNotSupportedException::new)
        );
    }

}
