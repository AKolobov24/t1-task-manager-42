package ru.t1.akolobov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.akolobov.tm.api.endpoint.IAuthEndpoint;
import ru.t1.akolobov.tm.api.endpoint.IProjectEndpoint;
import ru.t1.akolobov.tm.api.endpoint.ITaskEndpoint;
import ru.t1.akolobov.tm.dto.model.ProjectDTO;
import ru.t1.akolobov.tm.dto.model.TaskDTO;
import ru.t1.akolobov.tm.dto.request.*;
import ru.t1.akolobov.tm.dto.response.*;
import ru.t1.akolobov.tm.enumerated.Status;
import ru.t1.akolobov.tm.marker.IntegrationCategory;

import javax.xml.ws.soap.SOAPFaultException;
import java.util.ArrayList;
import java.util.List;

import static ru.t1.akolobov.tm.data.TestTask.createTask;
import static ru.t1.akolobov.tm.data.TestTask.createTaskList;

@Category(IntegrationCategory.class)
public class TaskEndpointTest {

    public static IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance("localhost", "6060");
    public static ITaskEndpoint taskEndpoint = ITaskEndpoint.newInstance("localhost", "6060");
    public static IProjectEndpoint projectEndpoint = IProjectEndpoint.newInstance("localhost", "6060");

    public static String adminToken;
    public static String userToken;
    public static List<TaskDTO> taskList = createTaskList(4);

    @BeforeClass
    public static void prepareSession() {
        adminToken = authEndpoint.login(new UserLoginRequest("akolobov", "akolobov")).getToken();
        createTasks(adminToken);
        userToken = authEndpoint.login(new UserLoginRequest("user1", "user1")).getToken();
        createTasks(userToken);
    }

    public static void createTasks(@Nullable String token) {
        TaskCreateRequest request = new TaskCreateRequest(token);
        for (int i = 0; i < 4; i++) {
            request.setName(taskList.get(i).getName());
            request.setDescription(taskList.get(i).getDescription());
            taskEndpoint.createTask(request);
        }
    }

    @AfterClass
    public static void closeSessions() {
        taskEndpoint.clearTask(new TaskClearRequest(userToken));
        taskEndpoint.clearTask(new TaskClearRequest(adminToken));
        authEndpoint.logout(new UserLogoutRequest(adminToken));
        authEndpoint.logout(new UserLogoutRequest(userToken));
        adminToken = null;
        userToken = null;
    }

    @Test
    public void bindToProject() {
        @NotNull TaskListResponse taskListResponse = taskEndpoint.listTask(
                new TaskListRequest(userToken)
        );
        @NotNull TaskDTO task = taskListResponse
                .getTaskList()
                .get(taskListResponse.getTaskList().size() - 1);

        @NotNull ProjectCreateRequest projectCreateRequest = new ProjectCreateRequest(userToken);
        projectCreateRequest.setName("task-bind_project");
        @NotNull ProjectCreateResponse projectCreateResponse =
                projectEndpoint.create(projectCreateRequest);
        ProjectDTO project = projectCreateResponse.getProject();
        Assert.assertNotNull(project);

        TaskBindToProjectRequest bindToProjectRequest = new TaskBindToProjectRequest(userToken);
        bindToProjectRequest.setProjectId(project.getId());
        bindToProjectRequest.setTaskId(task.getId());
        taskEndpoint.bindToProject(bindToProjectRequest);

        TaskGetByIdRequest getByIdRequest = new TaskGetByIdRequest(userToken);
        getByIdRequest.setId(task.getId());
        @NotNull TaskGetByIdResponse response = taskEndpoint.getTaskById(getByIdRequest);
        Assert.assertNotNull(response.getTask());
        Assert.assertEquals(project.getId(), response.getTask().getProjectId());

        bindToProjectRequest.setToken(adminToken);
        Assert.assertThrows(
                SOAPFaultException.class,
                () -> taskEndpoint.bindToProject(bindToProjectRequest)
        );
        bindToProjectRequest.setToken(userToken);
        bindToProjectRequest.setProjectId("INCORRECT_ID");
        Assert.assertThrows(
                SOAPFaultException.class,
                () -> taskEndpoint.bindToProject(bindToProjectRequest)
        );
        bindToProjectRequest.setProjectId(project.getId());
        bindToProjectRequest.setTaskId("INCORRECT_ID");
        Assert.assertThrows(
                SOAPFaultException.class,
                () -> taskEndpoint.bindToProject(bindToProjectRequest)
        );
        Assert.assertThrows(
                SOAPFaultException.class,
                () -> taskEndpoint.bindToProject(new TaskBindToProjectRequest())
        );

        @NotNull ProjectRemoveByIdRequest projectRemoveRequest =
                new ProjectRemoveByIdRequest(userToken);
        projectRemoveRequest.setId(project.getId());
        projectEndpoint.removeById(projectRemoveRequest);
    }

    @Test
    public void unbindFromProject() {
        @NotNull TaskListResponse taskListResponse = taskEndpoint.listTask(
                new TaskListRequest(userToken)
        );
        @NotNull TaskDTO task = taskListResponse
                .getTaskList()
                .get(taskListResponse.getTaskList().size() - 1);
        @NotNull ProjectCreateRequest projectCreateRequest = new ProjectCreateRequest(userToken);
        projectCreateRequest.setName("task-bind_project");
        @NotNull ProjectCreateResponse projectCreateResponse =
                projectEndpoint.create(projectCreateRequest);
        ProjectDTO project = projectCreateResponse.getProject();
        Assert.assertNotNull(project);

        TaskBindToProjectRequest bindToProjectRequest = new TaskBindToProjectRequest(userToken);
        bindToProjectRequest.setProjectId(project.getId());
        bindToProjectRequest.setTaskId(task.getId());
        taskEndpoint.bindToProject(bindToProjectRequest);

        TaskGetByIdRequest getByIdRequest = new TaskGetByIdRequest(userToken);
        getByIdRequest.setId(task.getId());
        @NotNull TaskGetByIdResponse response = taskEndpoint.getTaskById(getByIdRequest);
        Assert.assertNotNull(response.getTask());
        Assert.assertEquals(project.getId(), response.getTask().getProjectId());

        TaskUnbindFromProjectRequest unbindFromProjectRequest = new TaskUnbindFromProjectRequest(userToken);
        unbindFromProjectRequest.setId(task.getId());
        taskEndpoint.unbindFromProject(unbindFromProjectRequest);

        response = taskEndpoint.getTaskById(getByIdRequest);
        Assert.assertNotNull(response.getTask());
        Assert.assertNotEquals(project.getId(), response.getTask().getProjectId());
        Assert.assertNull(response.getTask().getProjectId());

        unbindFromProjectRequest.setToken(adminToken);
        Assert.assertThrows(
                SOAPFaultException.class,
                () -> taskEndpoint.unbindFromProject(unbindFromProjectRequest)
        );
        unbindFromProjectRequest.setToken(userToken);
        unbindFromProjectRequest.setId("INCORRECT_ID");
        Assert.assertThrows(
                SOAPFaultException.class,
                () -> taskEndpoint.unbindFromProject(unbindFromProjectRequest)
        );
        Assert.assertThrows(
                SOAPFaultException.class,
                () -> taskEndpoint.unbindFromProject(new TaskUnbindFromProjectRequest())
        );

        @NotNull ProjectRemoveByIdRequest projectRemoveRequest =
                new ProjectRemoveByIdRequest(userToken);
        projectRemoveRequest.setId(project.getId());
        projectEndpoint.removeById(projectRemoveRequest);
    }

    @Test
    public void getByProjectId() {
        @NotNull ProjectCreateRequest projectCreateRequest = new ProjectCreateRequest(userToken);
        projectCreateRequest.setName("task-bind_project");
        @NotNull ProjectCreateResponse projectCreateResponse =
                projectEndpoint.create(projectCreateRequest);
        ProjectDTO project = projectCreateResponse.getProject();
        Assert.assertNotNull(project);
        @NotNull TaskListResponse taskListResponse = taskEndpoint.listTask(
                new TaskListRequest(userToken)
        );
        TaskBindToProjectRequest bindToProjectRequest = new TaskBindToProjectRequest(userToken);
        bindToProjectRequest.setProjectId(project.getId());
        TaskDTO task;
        List<TaskDTO> taskList = new ArrayList<>();
        for (int i = 0; i < 2; i++) {
            task = taskListResponse.getTaskList().get(i);
            bindToProjectRequest.setTaskId(task.getId());
            taskEndpoint.bindToProject(bindToProjectRequest);
            task.setProjectId(project.getId());
            taskList.add(task);
        }
        TaskGetByProjectIdRequest getByProjectIdRequest = new TaskGetByProjectIdRequest(userToken);
        getByProjectIdRequest.setProjectId(project.getId());
        TaskGetByProjectIdResponse getByProjectIdResponse = taskEndpoint.getByProjectId(getByProjectIdRequest);
        Assert.assertFalse(getByProjectIdResponse.getTaskList().isEmpty());
        for (int i = 0; i < taskList.size(); i++) {
            Assert.assertEquals(taskList.get(i).getId(), getByProjectIdResponse.getTaskList().get(i).getId());
        }

        Assert.assertThrows(
                SOAPFaultException.class,
                () -> taskEndpoint.getByProjectId(new TaskGetByProjectIdRequest())
        );
        getByProjectIdRequest.setProjectId("INCORRECT_ID");
        Assert.assertTrue(taskEndpoint.getByProjectId(getByProjectIdRequest).getTaskList().isEmpty());
        getByProjectIdRequest.setProjectId("");
        Assert.assertThrows(
                SOAPFaultException.class,
                () -> taskEndpoint.getByProjectId(getByProjectIdRequest)
        );

        @NotNull ProjectRemoveByIdRequest projectRemoveRequest =
                new ProjectRemoveByIdRequest(userToken);
        projectRemoveRequest.setId(project.getId());
        projectEndpoint.removeById(projectRemoveRequest);
    }

    @Test
    public void changeStatusById() {
        @NotNull TaskListResponse taskListResponse = taskEndpoint.listTask(
                new TaskListRequest(adminToken)
        );
        @NotNull TaskDTO task = taskListResponse
                .getTaskList()
                .get(taskListResponse.getTaskList().size() - 1);
        @NotNull TaskChangeStatusByIdRequest request = new TaskChangeStatusByIdRequest(adminToken);
        request.setId(task.getId());
        request.setStatus(Status.IN_PROGRESS);
        taskEndpoint.changeTaskStatusById(request);
        @NotNull TaskGetByIdRequest getByIdRequest = new TaskGetByIdRequest(adminToken);
        getByIdRequest.setId(task.getId());
        @NotNull TaskGetByIdResponse response = taskEndpoint.getTaskById(getByIdRequest);
        Assert.assertNotNull(response.getTask());
        Assert.assertEquals(Status.IN_PROGRESS, response.getTask().getStatus());

        request.setToken(userToken);
        Assert.assertThrows(SOAPFaultException.class, () -> taskEndpoint.changeTaskStatusById(request));
        request.setToken(adminToken);
        request.setId("INCORRECT_ID");
        Assert.assertThrows(SOAPFaultException.class, () -> taskEndpoint.changeTaskStatusById(request));
        Assert.assertThrows(
                SOAPFaultException.class,
                () -> taskEndpoint.changeTaskStatusById(new TaskChangeStatusByIdRequest())
        );
    }

    @Test
    public void clear() {
        @NotNull TaskListRequest taskListRequest = new TaskListRequest(userToken);
        Assert.assertFalse(taskEndpoint.listTask(taskListRequest).getTaskList().isEmpty());
        taskEndpoint.clearTask(new TaskClearRequest(userToken));
        Assert.assertTrue(taskEndpoint.listTask(taskListRequest).getTaskList().isEmpty());
        Assert.assertFalse(taskEndpoint.listTask(new TaskListRequest(adminToken)).getTaskList().isEmpty());
        Assert.assertThrows(SOAPFaultException.class, () -> taskEndpoint.clearTask(new TaskClearRequest()));
        createTasks(userToken);
    }

    @Test
    public void completeById() {
        @NotNull TaskListResponse taskListResponse = taskEndpoint.listTask(
                new TaskListRequest(adminToken)
        );
        @NotNull TaskDTO task = taskListResponse
                .getTaskList()
                .get(taskListResponse.getTaskList().size() - 1);
        @NotNull TaskCompleteByIdRequest request = new TaskCompleteByIdRequest(adminToken);
        request.setId(task.getId());
        taskEndpoint.completeTaskById(request);
        @NotNull TaskGetByIdRequest getByIdRequest = new TaskGetByIdRequest(adminToken);
        getByIdRequest.setId(task.getId());
        @NotNull TaskGetByIdResponse response = taskEndpoint.getTaskById(getByIdRequest);
        Assert.assertNotNull(response.getTask());
        Assert.assertEquals(Status.COMPLETED, response.getTask().getStatus());

        request.setToken(userToken);
        Assert.assertThrows(SOAPFaultException.class, () -> taskEndpoint.completeTaskById(request));
        request.setToken(adminToken);
        request.setId("INCORRECT_ID");
        Assert.assertThrows(SOAPFaultException.class, () -> taskEndpoint.completeTaskById(request));
        Assert.assertThrows(
                SOAPFaultException.class,
                () -> taskEndpoint.completeTaskById(new TaskCompleteByIdRequest())
        );
    }

    @Test
    public void create() {
        @NotNull TaskDTO task = createTask();
        @NotNull TaskCreateRequest request = new TaskCreateRequest(userToken);
        request.setName(task.getName());
        request.setDescription(task.getDescription());

        @NotNull TaskCreateResponse response = taskEndpoint.createTask(request);
        Assert.assertNotNull(response.getTask());
        Assert.assertEquals(task.getName(), response.getTask().getName());
        Assert.assertEquals(task.getDescription(), response.getTask().getDescription());

        @NotNull TaskGetByIdRequest getByIdRequest = new TaskGetByIdRequest(userToken);
        getByIdRequest.setId(response.getTask().getId());
        @NotNull TaskGetByIdResponse getByIdResponse = taskEndpoint.getTaskById(getByIdRequest);
        Assert.assertNotNull(getByIdResponse.getTask());
        Assert.assertEquals(task.getName(), getByIdResponse.getTask().getName());
        Assert.assertEquals(task.getDescription(), getByIdResponse.getTask().getDescription());

        Assert.assertThrows(
                SOAPFaultException.class,
                () -> taskEndpoint.createTask(new TaskCreateRequest())
        );
    }

    @Test
    public void getById() {
        @NotNull TaskDTO task = createTask();
        @NotNull TaskCreateRequest createRequest = new TaskCreateRequest(userToken);
        createRequest.setName(task.getName());
        createRequest.setDescription(task.getDescription());

        @NotNull TaskCreateResponse createResponse = taskEndpoint.createTask(createRequest);
        Assert.assertNotNull(createResponse.getTask());

        @NotNull TaskGetByIdRequest getByIdRequest = new TaskGetByIdRequest(userToken);
        getByIdRequest.setId(createResponse.getTask().getId());
        @NotNull TaskGetByIdResponse getByIdResponse = taskEndpoint.getTaskById(getByIdRequest);

        Assert.assertNotNull(getByIdResponse.getTask());
        Assert.assertEquals(createResponse.getTask().getId(), getByIdResponse.getTask().getId());
        Assert.assertEquals(task.getName(), getByIdResponse.getTask().getName());
        Assert.assertEquals(task.getDescription(), getByIdResponse.getTask().getDescription());

        getByIdRequest.setToken(adminToken);
        Assert.assertNull(taskEndpoint.getTaskById(getByIdRequest).getTask());
        Assert.assertThrows(
                SOAPFaultException.class,
                () -> taskEndpoint.getTaskById(new TaskGetByIdRequest())
        );
    }

    @Test
    public void list() {
        TaskListRequest userTaskListRequest = new TaskListRequest(userToken);
        TaskListRequest adminTaskListRequest = new TaskListRequest(adminToken);
        int userTaskListSize = taskEndpoint
                .listTask(userTaskListRequest)
                .getTaskList().size();
        int adminTaskListSize = taskEndpoint
                .listTask(adminTaskListRequest)
                .getTaskList().size();

        @NotNull TaskDTO task = createTask();
        @NotNull TaskCreateRequest createRequest = new TaskCreateRequest(userToken);
        createRequest.setName(task.getName());
        createRequest.setDescription(task.getDescription());

        @NotNull TaskCreateResponse createResponse = taskEndpoint.createTask(createRequest);
        TaskDTO createdTask = createResponse.getTask();
        Assert.assertNotNull(createdTask);

        List<TaskDTO> taskList = taskEndpoint.listTask(userTaskListRequest).getTaskList();

        Assert.assertEquals(
                userTaskListSize + 1,
                taskList.size()
        );

        Assert.assertEquals(
                adminTaskListSize,
                taskEndpoint.listTask(adminTaskListRequest).getTaskList().size()
        );

        Assert.assertTrue(
                taskList.stream()
                        .anyMatch(m -> createdTask.getId().equals(m.getId()))
        );
        Assert.assertThrows(
                SOAPFaultException.class,
                () -> taskEndpoint.listTask(new TaskListRequest())
        );
    }

    @Test
    public void removeById() {
        @NotNull TaskListRequest userTaskListRequest = new TaskListRequest(userToken);
        TaskDTO task = taskEndpoint
                .listTask(userTaskListRequest)
                .getTaskList().get(0);
        Assert.assertNotNull(task);

        TaskRemoveByIdRequest taskRemoveByIdRequest = new TaskRemoveByIdRequest(userToken);
        taskRemoveByIdRequest.setId(task.getId());
        taskEndpoint.removeTaskById(taskRemoveByIdRequest);
        Assert.assertFalse(taskEndpoint
                .listTask(new TaskListRequest(userToken))
                .getTaskList()
                .contains(task)
        );
        Assert.assertThrows(
                SOAPFaultException.class,
                () -> taskEndpoint.removeTaskById(new TaskRemoveByIdRequest())
        );
    }

    @Test
    public void startById() {
        @NotNull TaskListResponse taskListResponse = taskEndpoint.listTask(
                new TaskListRequest(adminToken)
        );
        @NotNull TaskDTO task = taskListResponse
                .getTaskList()
                .get(taskListResponse.getTaskList().size() - 1);
        @NotNull TaskStartByIdRequest request = new TaskStartByIdRequest(adminToken);
        request.setId(task.getId());
        taskEndpoint.startTaskById(request);
        @NotNull TaskGetByIdRequest getByIdRequest = new TaskGetByIdRequest(adminToken);
        getByIdRequest.setId(task.getId());
        @NotNull TaskGetByIdResponse response = taskEndpoint.getTaskById(getByIdRequest);
        Assert.assertNotNull(response.getTask());
        Assert.assertEquals(Status.IN_PROGRESS, response.getTask().getStatus());

        request.setToken(userToken);
        Assert.assertThrows(SOAPFaultException.class, () -> taskEndpoint.startTaskById(request));
        request.setToken(adminToken);
        request.setId("INCORRECT_ID");
        Assert.assertThrows(SOAPFaultException.class, () -> taskEndpoint.startTaskById(request));
        Assert.assertThrows(
                SOAPFaultException.class,
                () -> taskEndpoint.startTaskById(new TaskStartByIdRequest())
        );
    }

    @Test
    public void updateById() {
        @NotNull TaskDTO task = createTask();
        @NotNull TaskCreateRequest createRequest = new TaskCreateRequest(userToken);
        createRequest.setName(task.getName());
        createRequest.setDescription(task.getDescription());
        @NotNull TaskCreateResponse createResponse = taskEndpoint.createTask(createRequest);
        Assert.assertNotNull(createResponse.getTask());

        @NotNull String name = "update-by-id-name";
        @NotNull String description = "update-by-id-desc";
        @NotNull TaskUpdateByIdRequest updateByIdRequest = new TaskUpdateByIdRequest(userToken);
        updateByIdRequest.setId(createResponse.getTask().getId());
        updateByIdRequest.setName(name);
        updateByIdRequest.setDescription(description);
        taskEndpoint.updateTaskById(updateByIdRequest);

        @NotNull TaskGetByIdRequest getByIdRequest = new TaskGetByIdRequest(userToken);
        getByIdRequest.setId(createResponse.getTask().getId());
        @NotNull TaskGetByIdResponse getByIdResponse = taskEndpoint.getTaskById(getByIdRequest);
        Assert.assertNotNull(getByIdResponse.getTask());
        Assert.assertEquals(name, getByIdResponse.getTask().getName());
        Assert.assertEquals(description, getByIdResponse.getTask().getDescription());

        Assert.assertThrows(
                SOAPFaultException.class,
                () -> taskEndpoint.updateTaskById(new TaskUpdateByIdRequest())
        );
        updateByIdRequest.setId(task.getId());
        Assert.assertThrows(SOAPFaultException.class,
                () -> taskEndpoint.updateTaskById(updateByIdRequest)
        );
    }

}
