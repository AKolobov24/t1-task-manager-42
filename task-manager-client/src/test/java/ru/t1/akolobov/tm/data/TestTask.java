package ru.t1.akolobov.tm.data;

import org.jetbrains.annotations.NotNull;
import ru.t1.akolobov.tm.dto.model.TaskDTO;

import java.util.ArrayList;
import java.util.List;

public final class TestTask {

    @NotNull
    public static TaskDTO createTask() {
        return new TaskDTO("new-task", "new-task-desc");
    }

    @NotNull
    public static List<TaskDTO> createTaskList(int size) {
        @NotNull List<TaskDTO> taskList = new ArrayList<>();
        for (int i = 1; i <= size; i++) {
            @NotNull TaskDTO task = new TaskDTO("task-" + i, "task-" + i + "desc");
            taskList.add(task);
        }
        return taskList;
    }


}
