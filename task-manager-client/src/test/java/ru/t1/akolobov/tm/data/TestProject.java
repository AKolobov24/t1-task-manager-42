package ru.t1.akolobov.tm.data;

import org.jetbrains.annotations.NotNull;
import ru.t1.akolobov.tm.dto.model.ProjectDTO;

import java.util.ArrayList;
import java.util.List;

public final class TestProject {

    @NotNull
    public static ProjectDTO createProject() {
        return new ProjectDTO("new-project", "new-project-desc");
    }

    @NotNull
    public static List<ProjectDTO> createProjectList(int size) {
        @NotNull List<ProjectDTO> projectList = new ArrayList<>();
        for (int i = 1; i <= size; i++) {
            @NotNull ProjectDTO project = new ProjectDTO("project-" + i, "project-" + i + "desc");
            projectList.add(project);
        }
        return projectList;
    }

}
