package ru.t1.akolobov.tm.data;

import org.jetbrains.annotations.NotNull;
import ru.t1.akolobov.tm.dto.model.UserDTO;

public final class TestUser {

    @NotNull
    public static final UserDTO NEW_USER = createUser();

    @NotNull
    private static UserDTO createUser() {
        UserDTO user = new UserDTO();
        user.setLogin("NEW_USER");
        user.setEmail("new_user@email.com");
        return user;
    }

}
