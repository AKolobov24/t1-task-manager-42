package ru.t1.akolobov.tm.data;

import org.jetbrains.annotations.NotNull;
import ru.t1.akolobov.tm.dto.model.ProjectDTO;

import java.util.ArrayList;
import java.util.List;

public final class TestProject {

    @NotNull
    public static ProjectDTO createProject() {
        return new ProjectDTO("new-project", "new-project-desc");
    }

    @NotNull
    public static ProjectDTO createProject(@NotNull final String userId) {
        @NotNull ProjectDTO project = createProject();
        project.setUserId(userId);
        return project;
    }

    @NotNull
    public static List<ProjectDTO> createProjectList(@NotNull final String userId) {
        @NotNull List<ProjectDTO> projectList = new ArrayList<>();
        for (int i = 1; i <= 5; i++) {
            @NotNull ProjectDTO project = new ProjectDTO("project-" + i, "project-" + i + "desc");
            project.setUserId(userId);
            projectList.add(project);
        }
        return projectList;
    }

}
