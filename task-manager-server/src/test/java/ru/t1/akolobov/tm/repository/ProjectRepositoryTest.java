package ru.t1.akolobov.tm.repository;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.akolobov.tm.api.repository.IProjectRepository;
import ru.t1.akolobov.tm.api.repository.IUserRepository;
import ru.t1.akolobov.tm.dto.model.ProjectDTO;
import ru.t1.akolobov.tm.marker.UnitCategory;
import ru.t1.akolobov.tm.service.ConnectionService;
import ru.t1.akolobov.tm.service.PropertyService;

import java.util.List;

import static ru.t1.akolobov.tm.data.TestProject.createProject;
import static ru.t1.akolobov.tm.data.TestProject.createProjectList;
import static ru.t1.akolobov.tm.data.TestUser.*;

@Category(UnitCategory.class)
public final class ProjectRepositoryTest {

    private static SqlSession connection;

    @BeforeClass
    public static void prepareConnection() {
        connection = new ConnectionService(new PropertyService()).getConnection();
        @NotNull IUserRepository userRepository = connection.getMapper(IUserRepository.class);
        userRepository.add(USER1);
        userRepository.add(USER2);
    }

    @AfterClass
    public static void closeConnection() {
        @NotNull IUserRepository userRepository = connection.getMapper(IUserRepository.class);
        userRepository.remove(USER1);
        userRepository.remove(USER2);
        connection.close();
    }

    @After
    public void clearData() {
        @NotNull IProjectRepository repository = connection.getMapper(IProjectRepository.class);
        repository.clear(USER1.getId());
        repository.clear(USER2.getId());
    }

    @Test
    public void add() {
        @NotNull IProjectRepository repository = connection.getMapper(IProjectRepository.class);
        @NotNull ProjectDTO project = createProject(USER1_ID);
        repository.add(project);
        Assert.assertTrue(repository.findAllByUserId(USER1_ID).contains(project));
        Assert.assertEquals(1, repository.findAllByUserId(USER1_ID).size());
    }

    @Test
    public void clear() {
        @NotNull IProjectRepository repository = connection.getMapper(IProjectRepository.class);
        List<ProjectDTO> projectList = createProjectList(USER1_ID);
        projectList.forEach(repository::add);
        int size = repository.getSize();
        repository.clear(USER1_ID);
        Assert.assertEquals(
                size - projectList.size(),
                repository.getSize().intValue()
        );
    }

    @Test
    public void existById() {
        @NotNull IProjectRepository repository = connection.getMapper(IProjectRepository.class);
        @NotNull ProjectDTO project = createProject(USER1_ID);
        repository.add(project);
        Assert.assertTrue(repository.existById(USER1_ID, project.getId()));
        Assert.assertFalse(repository.existById(USER2_ID, project.getId()));
    }

    @Test
    public void findAll() {
        @NotNull IProjectRepository repository = connection.getMapper(IProjectRepository.class);
        List<ProjectDTO> user1ProjectList = createProjectList(USER1_ID);
        List<ProjectDTO> user2ProjectList = createProjectList(USER2_ID);
        user1ProjectList.forEach(repository::add);
        user2ProjectList.forEach(repository::add);
        Assert.assertEquals(user1ProjectList, repository.findAllByUserId(USER1_ID));
        Assert.assertEquals(user2ProjectList, repository.findAllByUserId(USER2_ID));
    }

    @Test
    public void findOneById() {
        @NotNull IProjectRepository repository = connection.getMapper(IProjectRepository.class);
        @NotNull ProjectDTO project = createProject(USER1_ID);
        repository.add(project);
        @NotNull String projectId = project.getId();
        Assert.assertEquals(project, repository.findOneById(USER1_ID, projectId));
        Assert.assertNull(repository.findOneById(USER2_ID, projectId));
    }

    @Test
    public void getSize() {
        @NotNull IProjectRepository repository = connection.getMapper(IProjectRepository.class);
        List<ProjectDTO> projectList = createProjectList(USER1_ID);
        projectList.forEach(repository::add);
        Assert.assertEquals((Integer) projectList.size(), repository.getSizeByUserId(USER1_ID));
        repository.add(createProject(USER1_ID));
        Assert.assertEquals((Integer) (projectList.size() + 1), repository.getSizeByUserId(USER1_ID));
    }

    @Test
    public void remove() {
        @NotNull IProjectRepository repository = connection.getMapper(IProjectRepository.class);
        createProjectList(USER1_ID).forEach(repository::add);
        @NotNull ProjectDTO project = createProject(USER1_ID);
        repository.add(project);
        Assert.assertEquals(project, repository.findOneById(USER1_ID, project.getId()));
        repository.remove(project);
        Assert.assertNull(repository.findOneById(USER1_ID, project.getId()));
    }

    @Test
    public void removeById() {
        @NotNull IProjectRepository repository = connection.getMapper(IProjectRepository.class);
        createProjectList(USER1_ID).forEach(repository::add);
        @NotNull ProjectDTO project = createProject(USER1_ID);
        repository.add(project);
        repository.removeById(USER1_ID, project.getId());
        Assert.assertNull(repository.findOneById(USER1_ID, project.getId()));
    }

}
