package ru.t1.akolobov.tm.repository;

import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.akolobov.tm.api.repository.IProjectRepository;
import ru.t1.akolobov.tm.api.repository.ITaskRepository;
import ru.t1.akolobov.tm.api.repository.IUserRepository;
import ru.t1.akolobov.tm.dto.model.ProjectDTO;
import ru.t1.akolobov.tm.dto.model.TaskDTO;
import ru.t1.akolobov.tm.marker.UnitCategory;
import ru.t1.akolobov.tm.service.ConnectionService;
import ru.t1.akolobov.tm.service.PropertyService;

import java.util.List;

import static ru.t1.akolobov.tm.data.TestTask.createTask;
import static ru.t1.akolobov.tm.data.TestTask.createTaskList;
import static ru.t1.akolobov.tm.data.TestUser.*;

@Category(UnitCategory.class)
public final class TaskRepositoryTest {

    private static SqlSession connection;

    @BeforeClass
    @SneakyThrows
    public static void prepareConnection() {
        connection = new ConnectionService(new PropertyService()).getConnection();
        @NotNull IUserRepository userRepository = connection.getMapper(IUserRepository.class);
        userRepository.add(USER1);
        userRepository.add(USER2);
    }

    @AfterClass
    public static void closeConnection() {
        @NotNull IUserRepository userRepository = connection.getMapper(IUserRepository.class);
        userRepository.remove(USER1);
        userRepository.remove(USER2);
        connection.close();
    }

    @After
    @SneakyThrows
    public void clearData() {
        @NotNull ITaskRepository repository = connection.getMapper(ITaskRepository.class);
        repository.clear(USER1.getId());
        repository.clear(USER2.getId());
    }

    @Test
    public void add() {
        @NotNull ITaskRepository repository = connection.getMapper(ITaskRepository.class);
        @NotNull TaskDTO task = createTask(USER1_ID);
        repository.add(task);
        Assert.assertEquals(task, repository.findAllByUserId(USER1_ID).get(0));
        Assert.assertEquals(1, repository.findAllByUserId(USER1_ID).size());
    }

    @Test
    public void clear() {
        @NotNull ITaskRepository repository = connection.getMapper(ITaskRepository.class);
        List<TaskDTO> TaskList = createTaskList(USER1_ID);
        TaskList.forEach(repository::add);
        int size = repository.getSize();
        repository.clear(USER1_ID);
        Assert.assertEquals(
                size - TaskList.size(),
                repository.getSize().intValue()
        );
    }

    @Test
    public void existById() {
        @NotNull ITaskRepository repository = connection.getMapper(ITaskRepository.class);
        @NotNull TaskDTO task = createTask(USER1_ID);
        repository.add(task);
        Assert.assertTrue(repository.existById(USER1_ID, task.getId()));
        Assert.assertFalse(repository.existById(USER2_ID, task.getId()));
    }

    @Test
    public void findAll() {
        @NotNull ITaskRepository repository = connection.getMapper(ITaskRepository.class);
        List<TaskDTO> user1TaskList = createTaskList(USER1_ID);
        List<TaskDTO> user2TaskList = createTaskList(USER2_ID);
        user1TaskList.forEach(repository::add);
        user2TaskList.forEach(repository::add);
        Assert.assertEquals(user1TaskList, repository.findAllByUserId(USER1_ID));
        Assert.assertEquals(user2TaskList, repository.findAllByUserId(USER2_ID));
    }

    @Test
    public void findOneById() {
        @NotNull ITaskRepository repository = connection.getMapper(ITaskRepository.class);
        @NotNull TaskDTO task = createTask(USER1_ID);
        repository.add(task);
        @NotNull String taskId = task.getId();
        Assert.assertEquals(task, repository.findOneById(USER1_ID, taskId));
        Assert.assertNull(repository.findOneById(USER2_ID, taskId));
    }

    @Test
    public void getSize() {
        @NotNull ITaskRepository repository = connection.getMapper(ITaskRepository.class);
        List<TaskDTO> taskList = createTaskList(USER1_ID);
        taskList.forEach(repository::add);
        Assert.assertEquals((Integer) taskList.size(), repository.getSizeByUserId(USER1_ID));
        repository.add(createTask(USER1_ID));
        Assert.assertEquals((Integer) (taskList.size() + 1), repository.getSizeByUserId(USER1_ID));
    }

    @Test
    public void remove() {
        @NotNull ITaskRepository repository = connection.getMapper(ITaskRepository.class);
        createTaskList(USER1_ID).forEach(repository::add);
        @NotNull TaskDTO task = createTask(USER1_ID);
        repository.add(task);
        Assert.assertEquals(task, repository.findOneById(USER1_ID, task.getId()));
        repository.remove(task);
        Assert.assertNull(repository.findOneById(USER1_ID, task.getId()));
    }

    @Test
    public void removeById() {
        @NotNull ITaskRepository repository = connection.getMapper(ITaskRepository.class);
        createTaskList(USER1_ID).forEach(repository::add);
        @NotNull TaskDTO task = createTask(USER1_ID);
        repository.add(task);
        repository.removeById(USER1_ID, task.getId());
        Assert.assertNull(repository.findOneById(USER1_ID, task.getId()));
    }

    @Test
    public void findAllByProjectId() {
        @NotNull ITaskRepository repository = connection.getMapper(ITaskRepository.class);
        @NotNull IProjectRepository projectRepository = connection.getMapper(IProjectRepository.class);
        @NotNull ProjectDTO project = new ProjectDTO("test-project-for-task");
        project.setUserId(USER1_ID);
        projectRepository.add(project);
        String projectId = project.getId();
        List<TaskDTO> taskList = createTaskList(USER1_ID);
        taskList.forEach(t -> t.setProjectId(projectId));
        taskList.forEach(repository::add);
        repository.add(createTask(USER1_ID));
        Assert.assertEquals(taskList, repository.findAllByProjectId(USER1_ID, projectId));
        taskList.forEach(repository::remove);
        projectRepository.removeById(USER1_ID, projectId);
    }

}
