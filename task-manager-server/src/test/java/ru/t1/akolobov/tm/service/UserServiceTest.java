package ru.t1.akolobov.tm.service;

import org.apache.ibatis.session.LocalCacheScope;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.akolobov.tm.api.repository.IUserRepository;
import ru.t1.akolobov.tm.api.service.IConnectionService;
import ru.t1.akolobov.tm.api.service.IUserService;
import ru.t1.akolobov.tm.dto.model.UserDTO;
import ru.t1.akolobov.tm.enumerated.Role;
import ru.t1.akolobov.tm.exception.field.IdEmptyException;
import ru.t1.akolobov.tm.exception.user.*;
import ru.t1.akolobov.tm.marker.UnitCategory;
import ru.t1.akolobov.tm.util.HashUtil;

import java.util.List;

import static ru.t1.akolobov.tm.data.TestUser.*;

@Category(UnitCategory.class)
public final class UserServiceTest {

    @NotNull
    private final static PropertyService propertyService = new PropertyService();
    @NotNull
    private final static IConnectionService connectionService = new ConnectionService(propertyService);
    @NotNull
    private final static SqlSession repositoryConnection = connectionService.getConnection(true);
    @NotNull
    private static final IUserRepository userRepository =
            repositoryConnection.getMapper(IUserRepository.class);
    @NotNull
    private static final IUserService service = new UserService(connectionService, propertyService);

    private static List<UserDTO> userList;

    @BeforeClass
    public static void initRepository() {
        repositoryConnection.getConfiguration().setCacheEnabled(false);
        repositoryConnection.getConfiguration().setLocalCacheScope(LocalCacheScope.STATEMENT);
        userList = createUserList();
        userList.forEach(userRepository::add);
    }

    @AfterClass
    public static void clearRepository() {
        userList.forEach(user -> userRepository.removeById(user.getId()));
        repositoryConnection.close();
    }

    @Test
    public void create() {
        Assert.assertThrows(
                LoginEmptyException.class,
                () -> service.create(USER_EMPTY_ID, NEW_USER.getLogin(), Role.ADMIN)
        );
        Assert.assertThrows(
                PasswordEmptyException.class,
                () -> service.create(NEW_USER.getLogin(), USER_EMPTY_ID, Role.ADMIN)
        );
        Role role = null;
        Assert.assertThrows(
                RoleEmptyException.class,
                () -> service.create(NEW_USER.getLogin(), NEW_USER.getLogin(), role)
        );

        service.create(NEW_USER.getLogin(), NEW_USER.getLogin(), Role.ADMIN);
        NEW_USER.setPasswordHash(HashUtil.salt(propertyService, NEW_USER.getLogin()));
        @Nullable final UserDTO newUser = userRepository.findByLogin(NEW_USER.getLogin());
        Assert.assertNotNull(newUser);
        Assert.assertEquals(NEW_USER.getLogin(), newUser.getLogin());
        Assert.assertEquals(NEW_USER.getPasswordHash(), newUser.getPasswordHash());
        Assert.assertEquals(Role.ADMIN, newUser.getRole());
        Assert.assertThrows(
                LoginExistException.class,
                () -> service.create(NEW_USER.getLogin(), NEW_USER.getLogin(), Role.ADMIN)
        );
        userRepository.removeById(newUser.getId());
    }

    @Test
    public void removeByLogin() {
        userRepository.add(NEW_USER);
        final int size = userRepository.findAll().size();
        Assert.assertNotNull(userRepository.findByLogin(NEW_USER.getLogin()));
        service.removeByLogin(NEW_USER.getLogin());
        Assert.assertEquals(size - 1, userRepository.findAll().size());
        Assert.assertNull(userRepository.findByLogin(NEW_USER.getLogin()));

        Assert.assertThrows(
                LoginEmptyException.class,
                () -> service.removeByLogin(USER_EMPTY_ID)
        );
        Assert.assertThrows(
                UserNotFoundException.class,
                () -> service.removeByLogin(NEW_USER.getLogin())
        );
    }

    @Test
    public void removeByEmail() {
        userRepository.add(NEW_USER);
        final int size = userRepository.findAll().size();
        Assert.assertNotNull(userRepository.findByLogin(NEW_USER.getLogin()));
        service.removeByEmail(NEW_USER.getEmail());
        Assert.assertEquals(size - 1, userRepository.findAll().size());
        Assert.assertNull(userRepository.findByLogin(NEW_USER.getLogin()));

        Assert.assertThrows(
                EmailEmptyException.class,
                () -> service.removeByEmail(USER_EMPTY_ID)
        );
        Assert.assertThrows(
                UserNotFoundException.class,
                () -> service.removeByEmail(NEW_USER.getEmail())
        );
    }

    @Test
    public void setPassword() {
        @NotNull final UserDTO user = userRepository.findAll().get(0);
        @NotNull final String passwordHash = user.getPasswordHash();
        service.setPassword(user.getId(), "new_password");
        @Nullable final UserDTO updatedUser = userRepository.findOneById(user.getId());
        Assert.assertNotNull(updatedUser);
        Assert.assertNotEquals(passwordHash, updatedUser.getPasswordHash());
        Assert.assertThrows(
                IdEmptyException.class,
                () -> service.setPassword(USER_EMPTY_ID, "new_password")
        );
        Assert.assertThrows(
                PasswordEmptyException.class,
                () -> service.setPassword(user.getId(), "")
        );
        Assert.assertThrows(
                UserNotFoundException.class,
                () -> service.setPassword(USER2_ID, "new_password")
        );
    }

    @Test
    public void updateUser() {
        @NotNull final String firstName = "Firstname";
        @NotNull final String lastName = "LastName";
        @NotNull final String middleName = "MiddleName";
        @NotNull final UserDTO user = userRepository.findAll().get(0);
        service.updateUser(user.getId(), firstName, lastName, middleName);
        @Nullable final UserDTO updatedUser = userRepository.findOneById(user.getId());
        Assert.assertNotNull(updatedUser);
        Assert.assertEquals(updatedUser.getFirstName(), firstName);
        Assert.assertEquals(updatedUser.getLastName(), lastName);
        Assert.assertEquals(updatedUser.getMiddleName(), middleName);
        Assert.assertThrows(
                IdEmptyException.class,
                () -> service.updateUser(USER_EMPTY_ID, firstName, lastName, middleName)
        );
        Assert.assertThrows(
                FirstNameEmptyException.class,
                () -> service.updateUser(user.getId(), "", lastName, middleName)
        );
        Assert.assertThrows(
                LastNameEmptyException.class,
                () -> service.updateUser(user.getId(), firstName, "", middleName)
        );
        Assert.assertThrows(
                UserNotFoundException.class,
                () -> service.updateUser(USER2_ID, firstName, lastName, middleName)
        );
    }

    @Test
    public void lockUserByLogin() {
        @NotNull final UserDTO user = userRepository.findAll().get(0);
        Assert.assertFalse(user.isLocked());
        service.lockUserByLogin(user.getLogin());
        @Nullable final UserDTO lockedUser = userRepository.findOneById(user.getId());
        Assert.assertNotNull(lockedUser);
        Assert.assertTrue(lockedUser.isLocked());
        Assert.assertThrows(
                LoginEmptyException.class,
                () -> service.lockUserByLogin(USER_EMPTY_ID)
        );
        Assert.assertThrows(
                UserNotFoundException.class,
                () -> service.lockUserByLogin(USER2_ID)
        );
    }

    @Test
    public void unlockUserByLogin() {
        UserDTO user = userRepository.findAll().get(0);
        service.lockUserByLogin(user.getLogin());
        user = userRepository.findByLogin(user.getLogin());
        Assert.assertNotNull(user);
        Assert.assertTrue(user.isLocked());
        service.unlockUserByLogin(user.getLogin());
        @Nullable final UserDTO unlockedUser = userRepository.findOneById(user.getId());
        Assert.assertNotNull(unlockedUser);
        Assert.assertFalse(unlockedUser.isLocked());
        Assert.assertThrows(
                LoginEmptyException.class,
                () -> service.unlockUserByLogin(USER_EMPTY_ID)
        );
        Assert.assertThrows(
                UserNotFoundException.class,
                () -> service.unlockUserByLogin(USER2_ID)
        );
    }

    @Test
    public void findByLogin() {
        userRepository.add(NEW_USER);
        @Nullable final UserDTO user = service.findByLogin(NEW_USER.getLogin());
        Assert.assertNotNull(user);
        Assert.assertEquals(NEW_USER.getLogin(), user.getLogin());
        Assert.assertEquals(NEW_USER.getPasswordHash(), user.getPasswordHash());
        Assert.assertEquals(NEW_USER.getRole(), user.getRole());
        Assert.assertThrows(
                LoginEmptyException.class,
                () -> service.findByLogin(USER_EMPTY_ID)
        );
        Assert.assertNull(service.findByLogin("NotExistingLogin"));
        userRepository.removeById(user.getId());
    }

    @Test
    public void findByEmail() {
        userRepository.add(NEW_USER);
        @Nullable final UserDTO user = service.findByEmail(NEW_USER.getEmail());
        Assert.assertNotNull(user);
        Assert.assertEquals(NEW_USER, user);
        Assert.assertThrows(
                EmailEmptyException.class,
                () -> service.findByEmail(USER_EMPTY_ID)
        );
        Assert.assertNull(service.findByEmail("NotExistingEmail"));
        userRepository.removeById(user.getId());
    }

    @Test
    public void isLoginExist() {
        userRepository.add(NEW_USER);
        Assert.assertTrue(service.isLoginExist(NEW_USER.getLogin()));
        Assert.assertThrows(
                LoginEmptyException.class,
                () -> service.isLoginExist(USER_EMPTY_ID)
        );
        Assert.assertFalse(service.isLoginExist("NotExistingLogin"));
        userRepository.removeById(NEW_USER.getId());
    }

    @Test
    public void isEmailExist() {
        userRepository.add(NEW_USER);
        Assert.assertTrue(service.isEmailExist(NEW_USER.getEmail()));
        Assert.assertThrows(
                EmailEmptyException.class,
                () -> service.isEmailExist(USER_EMPTY_ID)
        );
        Assert.assertFalse(service.isEmailExist("NotExistingEmail"));
        userRepository.removeById(NEW_USER.getId());
    }

}
