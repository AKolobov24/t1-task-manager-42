package ru.t1.akolobov.tm.data;

import org.jetbrains.annotations.NotNull;
import ru.t1.akolobov.tm.dto.model.SessionDTO;

import java.util.ArrayList;
import java.util.List;

public final class TestSession {

    @NotNull
    public static SessionDTO createSession(@NotNull final String userId) {
        @NotNull SessionDTO session = new SessionDTO();
        session.setUserId(userId);
        return session;
    }

    @NotNull
    public static List<SessionDTO> createSessionList(@NotNull final String userId) {
        @NotNull List<SessionDTO> sessionList = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            @NotNull SessionDTO session = new SessionDTO();
            session.setUserId(userId);
            sessionList.add(session);
        }
        return sessionList;
    }

}
