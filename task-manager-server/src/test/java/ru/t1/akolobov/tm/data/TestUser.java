package ru.t1.akolobov.tm.data;

import org.jetbrains.annotations.NotNull;
import ru.t1.akolobov.tm.dto.model.UserDTO;
import ru.t1.akolobov.tm.util.HashUtil;

import java.util.ArrayList;
import java.util.List;

public final class TestUser {

    public static final UserDTO USER1 = createUser("TEST_USER_1");
    public static final UserDTO USER2 = createUser("TEST_USER_2");
    public static final UserDTO NEW_USER = createUser("TEST_NEW_USER");
    public static final String USER_EMPTY_ID = "";
    public static final String USER1_ID = USER1.getId();
    public static final String USER2_ID = USER2.getId();

    @NotNull
    public static UserDTO createUser(@NotNull String login) {
        @NotNull UserDTO user = new UserDTO();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(login, 10, "secret"));
        user.setEmail(login + "@email.ru");
        return user;
    }

    @NotNull
    public static List<UserDTO> createUserList() {
        @NotNull List<UserDTO> userList = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            @NotNull UserDTO user = new UserDTO();
            @NotNull String login = "TEST_USER_" + i;
            user.setLogin(login);
            user.setPasswordHash(HashUtil.salt(login, 10, "secret"));
            user.setEmail(login + "@email.ru");
            userList.add(user);
        }
        return userList;
    }

}
