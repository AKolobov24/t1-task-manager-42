package ru.t1.akolobov.tm.data;

import org.jetbrains.annotations.NotNull;
import ru.t1.akolobov.tm.dto.model.TaskDTO;

import java.util.ArrayList;
import java.util.List;

public final class TestTask {

    @NotNull
    public static TaskDTO createTask() {
        return new TaskDTO("task-1", "task-1-desc");
    }

    @NotNull
    public static TaskDTO createTask(@NotNull final String userId) {
        @NotNull TaskDTO task = createTask();
        task.setUserId(userId);
        return task;
    }

    @NotNull
    public static List<TaskDTO> createTaskList(@NotNull final String userId) {
        @NotNull List<TaskDTO> taskList = new ArrayList<>();
        for (int i = 1; i <= 5; i++) {
            @NotNull TaskDTO task = new TaskDTO("task-" + i, "task-" + i + "-desc");
            task.setUserId(userId);
            taskList.add(task);
        }
        return taskList;
    }

}
