package ru.t1.akolobov.tm.service;

import org.apache.ibatis.session.LocalCacheScope;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.akolobov.tm.api.repository.ISessionRepository;
import ru.t1.akolobov.tm.api.repository.IUserRepository;
import ru.t1.akolobov.tm.api.service.IConnectionService;
import ru.t1.akolobov.tm.api.service.ISessionService;
import ru.t1.akolobov.tm.dto.model.SessionDTO;
import ru.t1.akolobov.tm.exception.field.IdEmptyException;
import ru.t1.akolobov.tm.exception.field.UserIdEmptyException;
import ru.t1.akolobov.tm.marker.UnitCategory;

import java.util.List;

import static ru.t1.akolobov.tm.data.TestSession.createSession;
import static ru.t1.akolobov.tm.data.TestSession.createSessionList;
import static ru.t1.akolobov.tm.data.TestUser.*;

@Category(UnitCategory.class)
public class SessionServiceTest {

    @NotNull
    private final static PropertyService propertyService = new PropertyService();
    @NotNull
    private final static IConnectionService connectionService = new ConnectionService(propertyService);
    @NotNull
    private final static SqlSession repositoryConnection = connectionService.getConnection(true);
    @NotNull
    private final static IUserRepository userRepository = repositoryConnection.getMapper(IUserRepository.class);
    @NotNull
    private final ISessionRepository repository = repositoryConnection.getMapper(ISessionRepository.class);
    @NotNull
    private final ISessionService service = new SessionService(connectionService);

    @BeforeClass
    public static void addUsers() {
        repositoryConnection.getConfiguration().setCacheEnabled(false);
        repositoryConnection.getConfiguration().setLocalCacheScope(LocalCacheScope.STATEMENT);
        userRepository.add(USER1);
        userRepository.add(USER2);
    }

    @AfterClass
    public static void clearUsers() {
        userRepository.remove(USER1);
        userRepository.remove(USER2);
        repositoryConnection.close();
    }

    @Before
    public void initRepository() {
        createSessionList(USER1_ID).forEach(repository::add);
    }

    @After
    public void clearRepository() {
        repository.clear(USER1_ID);
        repository.clear(USER2_ID);
    }

    @Test
    public void add() {
        SessionDTO session = createSession(USER1_ID);
        Object result = service.add(USER1_ID, session);
        Assert.assertNotNull(result);
        Assert.assertEquals(session, result);
        Assert.assertEquals(session, repository.findOneById(session.getUserId(), session.getId()));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.add(USER_EMPTY_ID, session));
    }

    @Test
    public void clear() {
        Assert.assertThrows(UserIdEmptyException.class, () -> service.clear(USER_EMPTY_ID));
        List<SessionDTO> sessionList = createSessionList(USER2_ID);
        service.add(sessionList);
        Assert.assertFalse(service.findAll(USER2_ID).isEmpty());
        service.clear(USER2_ID);
        Assert.assertTrue(service.findAll(USER2_ID).isEmpty());
    }

    @Test
    public void existById() {
        SessionDTO session = createSession(USER1_ID);
        service.add(session);
        Assert.assertTrue(service.existById(USER1_ID, session.getId()));
        Assert.assertFalse(service.existById(USER2_ID, session.getId()));
        Assert.assertThrows(IdEmptyException.class, () -> service.existById(USER1_ID, USER_EMPTY_ID));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.existById(USER_EMPTY_ID, session.getId()));
    }

    @Test
    public void findAll() {
        List<SessionDTO> sessionList = createSessionList(USER2_ID);
        service.add(sessionList);
        Assert.assertEquals(sessionList, service.findAll(USER2_ID));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.findAll(USER_EMPTY_ID));
    }

    @Test
    public void findOneById() {
        @NotNull SessionDTO session = createSession(USER1_ID);
        service.add(session);
        Assert.assertEquals(session, service.findOneById(USER1_ID, session.getId()));
        Assert.assertThrows(
                UserIdEmptyException.class,
                () -> service.findOneById(USER_EMPTY_ID, session.getId())
        );
        Assert.assertThrows(
                IdEmptyException.class,
                () -> service.findOneById(USER1_ID, USER_EMPTY_ID)
        );
    }

    @Test
    public void getSize() {
        int size = service.findAll(USER1_ID).size();
        Assert.assertEquals(size, service.getSize(USER1_ID).intValue());
        service.add(createSession(USER1_ID));
        Assert.assertEquals(size + 1, service.getSize(USER1_ID).intValue());
        Assert.assertThrows(UserIdEmptyException.class, () -> service.getSize(USER_EMPTY_ID));
    }

    @Test
    public void remove() {
        @NotNull final List<SessionDTO> sessionList = service.findAll(USER1_ID);
        int size = sessionList.size();
        @NotNull final SessionDTO session = sessionList.get(size - 1);
        Assert.assertNotNull(session);
        service.remove(USER1_ID, session);
        Assert.assertFalse(service.findAll(USER1_ID).contains(session));
        Assert.assertNull(service.findOneById(USER1_ID, session.getId()));
        Assert.assertEquals(size - 1, service.findAll(USER1_ID).size());
        Assert.assertThrows(UserIdEmptyException.class, () -> service.remove(USER_EMPTY_ID, session));
    }

    @Test
    public void removeById() {
        @NotNull final List<SessionDTO> sessionList = service.findAll(USER1_ID);
        int size = sessionList.size();
        @NotNull final SessionDTO session = sessionList.get(size - 1);
        Assert.assertNotNull(session);
        service.removeById(USER1_ID, session.getId());
        Assert.assertFalse(service.findAll(USER1_ID).contains(session));
        Assert.assertNull(service.findOneById(USER1_ID, session.getId()));
        Assert.assertEquals(size - 1, service.findAll(USER1_ID).size());
        Assert.assertThrows(UserIdEmptyException.class, () -> service.removeById(USER_EMPTY_ID, session.getId()));
        Assert.assertThrows(IdEmptyException.class, () -> service.removeById(USER1_ID, USER_EMPTY_ID));
    }

}
