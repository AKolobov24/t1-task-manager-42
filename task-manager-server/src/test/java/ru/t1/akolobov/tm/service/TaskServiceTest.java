package ru.t1.akolobov.tm.service;

import lombok.SneakyThrows;
import org.apache.ibatis.session.LocalCacheScope;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.akolobov.tm.api.repository.IProjectRepository;
import ru.t1.akolobov.tm.api.repository.ITaskRepository;
import ru.t1.akolobov.tm.api.repository.IUserRepository;
import ru.t1.akolobov.tm.api.service.IConnectionService;
import ru.t1.akolobov.tm.api.service.ITaskService;
import ru.t1.akolobov.tm.data.TestProject;
import ru.t1.akolobov.tm.dto.model.ProjectDTO;
import ru.t1.akolobov.tm.dto.model.TaskDTO;
import ru.t1.akolobov.tm.enumerated.Sort;
import ru.t1.akolobov.tm.enumerated.Status;
import ru.t1.akolobov.tm.exception.entity.TaskNotFoundException;
import ru.t1.akolobov.tm.exception.field.*;
import ru.t1.akolobov.tm.marker.UnitCategory;

import java.util.ArrayList;
import java.util.List;

import static ru.t1.akolobov.tm.data.TestTask.createTask;
import static ru.t1.akolobov.tm.data.TestTask.createTaskList;
import static ru.t1.akolobov.tm.data.TestUser.*;

@Category(UnitCategory.class)
public class TaskServiceTest {

    @NotNull
    private final static PropertyService propertyService = new PropertyService();
    @NotNull
    private final static IConnectionService connectionService = new ConnectionService(propertyService);
    @NotNull
    private final static SqlSession repositoryConnection = connectionService.getConnection(true);
    @NotNull
    private final static IUserRepository userRepository = repositoryConnection.getMapper(IUserRepository.class);
    @NotNull
    private final ITaskRepository repository = repositoryConnection.getMapper(ITaskRepository.class);
    @NotNull
    private final IProjectRepository projectRepository = repositoryConnection.getMapper(IProjectRepository.class);
    @NotNull
    private final ITaskService service = new TaskService(connectionService);

    @BeforeClass
    public static void addUsers() {
        repositoryConnection.getConfiguration().setCacheEnabled(false);
        repositoryConnection.getConfiguration().setLocalCacheScope(LocalCacheScope.STATEMENT);
        userRepository.add(USER1);
        userRepository.add(USER2);
    }

    @AfterClass
    public static void clearUsers() {
        userRepository.remove(USER1);
        userRepository.remove(USER2);
        repositoryConnection.close();
    }

    @Before
    public void initRepository() {
        createTaskList(USER1_ID).forEach(repository::add);
    }

    @After
    public void clearRepository() {
        repository.clear(USER1_ID);
        repository.clear(USER2_ID);
        projectRepository.clear(USER2_ID);
    }

    @Test
    public void add() {
        TaskDTO task = createTask(USER1_ID);
        Object result = service.add(USER1_ID, task);
        Assert.assertNotNull(result);
        Assert.assertEquals(task, result);
        Assert.assertEquals(task, repository.findOneById(task.getUserId(), task.getId()));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.add(USER_EMPTY_ID, task));
    }

    @Test
    public void clear() {
        Assert.assertThrows(UserIdEmptyException.class, () -> service.clear(USER_EMPTY_ID));
        List<TaskDTO> taskList = createTaskList(USER2_ID);
        service.add(taskList);
        Assert.assertFalse(service.findAll(USER2_ID).isEmpty());
        service.clear(USER2_ID);
        Assert.assertTrue(service.findAll(USER2_ID).isEmpty());
    }

    @Test
    public void existById() {
        TaskDTO task = createTask(USER1_ID);
        service.add(task);
        Assert.assertTrue(service.existById(USER1_ID, task.getId()));
        Assert.assertFalse(service.existById(USER2_ID, task.getId()));
        Assert.assertThrows(IdEmptyException.class, () -> service.existById(USER1_ID, USER_EMPTY_ID));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.existById(USER_EMPTY_ID, task.getId()));
    }

    @Test
    public void findAll() {
        List<TaskDTO> taskList = createTaskList(USER2_ID);
        service.add(taskList);
        Assert.assertEquals(taskList, service.findAll(USER2_ID));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.findAll(USER_EMPTY_ID));
    }


    @Test
    public void findAllSorted() {
        @NotNull TaskDTO task = createTask(USER1_ID);
        task.setName("task-0");
        task.setDescription("task-0-desc");
        @NotNull List<TaskDTO> taskList = new ArrayList<>();
        taskList.add(task);
        taskList.addAll(service.findAll(USER1_ID));
        service.add(task);
        Assert.assertEquals(taskList, service.findAll(USER1_ID, Sort.BY_NAME));
        Assert.assertThrows(
                UserIdEmptyException.class,
                () -> service.findAll(USER_EMPTY_ID, Sort.BY_NAME)
        );
    }

    @Test
    public void findOneById() {
        @NotNull TaskDTO task = createTask(USER1_ID);
        service.add(task);
        Assert.assertEquals(task, service.findOneById(USER1_ID, task.getId()));
        Assert.assertThrows(
                UserIdEmptyException.class,
                () -> service.findOneById(USER_EMPTY_ID, task.getId())
        );
        Assert.assertThrows(
                IdEmptyException.class,
                () -> service.findOneById(USER1_ID, USER_EMPTY_ID)
        );
    }

    @Test
    public void getSize() {
        int size = service.findAll(USER1_ID).size();
        Assert.assertEquals(size, service.getSize(USER1_ID).intValue());
        service.add(createTask(USER1_ID));
        Assert.assertEquals(size + 1, service.getSize(USER1_ID).intValue());
        Assert.assertThrows(UserIdEmptyException.class, () -> service.getSize(USER_EMPTY_ID));
    }

    @Test
    public void remove() {
        @NotNull final List<TaskDTO> taskList = service.findAll(USER1_ID);
        int size = taskList.size();
        @NotNull final TaskDTO task = taskList.get(0);
        Assert.assertNotNull(task);
        service.remove(USER1_ID, task);
        Assert.assertFalse(service.findAll(USER1_ID).contains(task));
        Assert.assertNull(service.findOneById(USER1_ID, task.getId()));
        Assert.assertEquals(size - 1, service.findAll(USER1_ID).size());
        Assert.assertThrows(UserIdEmptyException.class, () -> service.remove(USER_EMPTY_ID, task));
    }

    @Test
    public void removeById() {
        @NotNull final List<TaskDTO> taskList = service.findAll(USER1_ID);
        int size = taskList.size();
        @NotNull final TaskDTO task = taskList.get(0);
        Assert.assertNotNull(task);
        service.removeById(USER1_ID, task.getId());
        Assert.assertFalse(service.findAll(USER1_ID).contains(task));
        Assert.assertNull(service.findOneById(USER1_ID, task.getId()));
        Assert.assertEquals(size - 1, service.findAll(USER1_ID).size());
        Assert.assertThrows(UserIdEmptyException.class, () -> service.removeById(USER_EMPTY_ID, task.getId()));
        Assert.assertThrows(IdEmptyException.class, () -> service.removeById(USER1_ID, USER_EMPTY_ID));
    }

    @Test
    public void changeStatusById() {
        TaskDTO task = service.add(USER1_ID, createTask(USER1_ID));
        Assert.assertNotNull(task);
        String newTaskId = task.getId();
        Assert.assertNotNull(service.changeStatusById(USER1_ID, newTaskId, Status.IN_PROGRESS));
        task = service.findOneById(USER1_ID, newTaskId);
        Assert.assertNotNull(task);
        Assert.assertEquals(Status.IN_PROGRESS, task.getStatus());
        Assert.assertThrows(
                UserIdEmptyException.class,
                () -> service.changeStatusById(USER_EMPTY_ID, newTaskId, Status.IN_PROGRESS)
        );
        Assert.assertThrows(
                IdEmptyException.class,
                () -> service.changeStatusById(USER1_ID, USER_EMPTY_ID, Status.IN_PROGRESS)
        );
        Assert.assertThrows(
                StatusEmptyException.class,
                () -> service.changeStatusById(USER1_ID, newTaskId, null)
        );
        Assert.assertThrows(
                TaskNotFoundException.class,
                () -> service.changeStatusById(USER2_ID, newTaskId, Status.IN_PROGRESS)
        );
    }

    @Test
    public void create() {
        TaskDTO task = createTask(USER1_ID);
        TaskDTO newTask = service.create(USER1_ID, task.getName());
        Assert.assertNotNull(newTask);
        newTask = service.findOneById(USER1_ID, newTask.getId());
        Assert.assertNotNull(newTask);
        Assert.assertEquals(task.getName(), newTask.getName());

        newTask = service.create(USER1_ID, task.getName(), task.getDescription());
        Assert.assertNotNull(newTask);
        newTask = service.findOneById(USER1_ID, newTask.getId());
        Assert.assertNotNull(newTask);
        Assert.assertEquals(task.getName(), newTask.getName());
        Assert.assertEquals(task.getDescription(), newTask.getDescription());

        task.setStatus(Status.IN_PROGRESS);
        newTask = service.create(USER1_ID, task.getName(), task.getStatus());
        Assert.assertNotNull(newTask);
        newTask = service.findOneById(USER1_ID, newTask.getId());
        Assert.assertNotNull(newTask);
        Assert.assertEquals(task.getName(), newTask.getName());
        Assert.assertEquals(task.getStatus(), newTask.getStatus());

        Assert.assertThrows(
                UserIdEmptyException.class,
                () -> service.create(USER_EMPTY_ID, task.getName())
        );
        Assert.assertThrows(
                NameEmptyException.class,
                () -> service.create(USER1_ID, "")
        );
    }

    @Test
    public void updateById() {
        TaskDTO task = service.findAll(USER1_ID).get(0);
        Assert.assertNotNull(task);
        String newName = "NewName";
        String newDescription = "NewDescription";
        service.updateById(USER1_ID, task.getId(), newName, newDescription);
        TaskDTO newTask = service.findOneById(USER1_ID, task.getId());
        Assert.assertNotNull(newTask);
        Assert.assertEquals(newName, newTask.getName());
        Assert.assertEquals(newDescription, newTask.getDescription());

        Assert.assertThrows(
                UserIdEmptyException.class,
                () -> service.updateById(USER_EMPTY_ID, task.getId(), newName, newDescription)
        );
        Assert.assertThrows(
                IdEmptyException.class,
                () -> service.updateById(USER1_ID, USER_EMPTY_ID, newName, newDescription)
        );
        Assert.assertThrows(
                NameEmptyException.class,
                () -> service.updateById(USER1_ID, task.getId(), "", newDescription)
        );
        Assert.assertThrows(
                TaskNotFoundException.class,
                () -> service.updateById(USER2_ID, task.getId(), newName, newDescription)
        );
    }

    @Test
    @SneakyThrows
    public void findAllByProjectId() {
        ProjectDTO project = TestProject.createProject(USER2_ID);
        projectRepository.add(project);
        repositoryConnection.commit();
        List<TaskDTO> taskList = createTaskList(USER2_ID);
        taskList.forEach((t) -> t.setProjectId(project.getId()));
        service.add(taskList);
        service.add(USER2_ID, createTask(USER2_ID));
        Assert.assertEquals(taskList, service.findAllByProjectId(USER2_ID, project.getId()));
        Assert.assertThrows(
                UserIdEmptyException.class,
                () -> service.findAllByProjectId(USER_EMPTY_ID, project.getId())
        );
        Assert.assertThrows(
                ProjectIdEmptyException.class,
                () -> service.findAllByProjectId(USER2_ID, USER_EMPTY_ID)
        );
    }

}
