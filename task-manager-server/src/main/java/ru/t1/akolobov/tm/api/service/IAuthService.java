package ru.t1.akolobov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.akolobov.tm.dto.model.SessionDTO;

public interface IAuthService {

    String login(@Nullable String login, @Nullable String password);

    void logout(@Nullable String token);

    @NotNull
    SessionDTO validateToken(@NotNull String token);

}
