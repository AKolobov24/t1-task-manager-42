package ru.t1.akolobov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.akolobov.tm.dto.model.TaskDTO;
import ru.t1.akolobov.tm.enumerated.Sort;
import ru.t1.akolobov.tm.enumerated.Status;

import java.util.Collection;
import java.util.List;

public interface ITaskService {

    @NotNull TaskDTO add(@Nullable TaskDTO task);

    @NotNull
    TaskDTO add(@Nullable String userId, @Nullable TaskDTO task);

    @NotNull
    TaskDTO update(@Nullable String userId, @Nullable TaskDTO task);

    void clear(@Nullable String userId);

    void clear();

    boolean existById(@Nullable String userId, @Nullable String id);

    @NotNull
    List<TaskDTO> findAll();

    @NotNull
    List<TaskDTO> findAll(@Nullable String userId);

    @NotNull
    List<TaskDTO> findAll(@Nullable String userId, @Nullable Sort sort);

    @Nullable
    TaskDTO findOneById(@Nullable String userId, @Nullable String id);

    @NotNull
    Integer getSize(@Nullable String userId);

    @NotNull
    TaskDTO remove(@Nullable String userId, @Nullable TaskDTO task);

    void removeById(@Nullable String userId, @Nullable String id);

    @NotNull
    TaskDTO changeStatusById(@Nullable String userId, @Nullable String id, Status status);

    @Nullable
    TaskDTO create(@Nullable String userId, @Nullable String name);

    @Nullable
    TaskDTO create(@Nullable String userId, @Nullable String name, @Nullable String description);

    @Nullable
    TaskDTO create(@Nullable String userId, @Nullable String name, @Nullable Status status);

    @NotNull
    Collection<TaskDTO> add(@NotNull Collection<TaskDTO> models);

    @NotNull
    Collection<TaskDTO> set(@NotNull Collection<TaskDTO> models);

    @NotNull
    List<TaskDTO> findAllByProjectId(@Nullable String userId, @Nullable String projectId);

    @NotNull
    TaskDTO updateById(@Nullable String userId, @Nullable String id, @Nullable String name, @Nullable String description);

}
