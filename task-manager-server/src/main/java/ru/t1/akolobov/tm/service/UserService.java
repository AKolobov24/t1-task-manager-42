package ru.t1.akolobov.tm.service;

import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.akolobov.tm.api.repository.IProjectRepository;
import ru.t1.akolobov.tm.api.repository.ITaskRepository;
import ru.t1.akolobov.tm.api.repository.IUserRepository;
import ru.t1.akolobov.tm.api.service.IConnectionService;
import ru.t1.akolobov.tm.api.service.IPropertyService;
import ru.t1.akolobov.tm.api.service.IUserService;
import ru.t1.akolobov.tm.dto.model.UserDTO;
import ru.t1.akolobov.tm.enumerated.Role;
import ru.t1.akolobov.tm.enumerated.Sort;
import ru.t1.akolobov.tm.exception.entity.EntityNotFoundException;
import ru.t1.akolobov.tm.exception.field.IdEmptyException;
import ru.t1.akolobov.tm.exception.user.*;
import ru.t1.akolobov.tm.util.HashUtil;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

public final class UserService extends AbstractService implements IUserService {

    @NotNull
    private final IPropertyService propertyService;

    public UserService(
            @NotNull IConnectionService connectionService,
            @NotNull IPropertyService propertyService
    ) {
        super(connectionService);
        this.propertyService = propertyService;
    }

    @NotNull
    @Override
    public UserDTO add(@Nullable final UserDTO user) {
        if (user == null) throw new EntityNotFoundException();
        @NotNull SqlSession connection = connectionService.getConnection();
        try {
            @NotNull final IUserRepository repository =
                    connection.getMapper(IUserRepository.class);
            repository.add(user);
            connection.commit();
            return user;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    public UserDTO create(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new LoginExistException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @NotNull final UserDTO user = new UserDTO();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        @NotNull final SqlSession connection = connectionService.getConnection();
        try {
            @NotNull final IUserRepository repository =
                    connection.getMapper(IUserRepository.class);
            repository.add(user);
            connection.commit();
            return user;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    public UserDTO create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new LoginExistException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        @NotNull final UserDTO user = new UserDTO();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setEmail(email);
        @NotNull final SqlSession connection = connectionService.getConnection();
        try {
            @NotNull final IUserRepository repository =
                    connection.getMapper(IUserRepository.class);
            repository.add(user);
            connection.commit();
            return user;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    public UserDTO create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final Role role
    ) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new LoginExistException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (role == null) throw new RoleEmptyException();
        @NotNull final UserDTO user = new UserDTO();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setRole(role);
        @NotNull final SqlSession connection = connectionService.getConnection();
        try {
            @NotNull final IUserRepository repository =
                    connection.getMapper(IUserRepository.class);
            repository.add(user);
            connection.commit();
            return user;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Nullable
    @Override
    public UserDTO findByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        try (@NotNull final SqlSession connection = connectionService.getConnection()) {
            @NotNull IUserRepository repository =
                    connection.getMapper(IUserRepository.class);
            return repository.findByLogin(login);
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public UserDTO findByEmail(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        try (@NotNull final SqlSession connection = connectionService.getConnection()) {
            @NotNull IUserRepository repository =
                    connection.getMapper(IUserRepository.class);
            return repository.findByEmail(email);
        }
    }

    @NotNull
    @Override
    public UserDTO removeByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        return remove(
                Optional.ofNullable(findByLogin(login))
                        .orElseThrow(UserNotFoundException::new)
        );
    }

    @NotNull
    @Override
    public UserDTO removeByEmail(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        return remove(
                Optional.ofNullable(findByEmail(email))
                        .orElseThrow(UserNotFoundException::new)
        );
    }

    @NotNull
    @Override
    public UserDTO remove(@Nullable UserDTO user) {
        if (user == null) throw new UserNotFoundException();
        @NotNull final String userId = user.getId();
        @NotNull final SqlSession connection = connectionService.getConnection();
        try {
            @NotNull final IUserRepository repository =
                    connection.getMapper(IUserRepository.class);
            repository.remove(user);
            connection.getMapper(IProjectRepository.class).clear(userId);
            connection.getMapper(ITaskRepository.class).clear(userId);
            connection.commit();
            return user;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    public int removeById(@Nullable String id) {
        return 0;
    }

    @NotNull
    @Override
    public UserDTO setPassword(@Nullable final String id, @Nullable final String password) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @NotNull final SqlSession connection = connectionService.getConnection();
        try {
            @NotNull final IUserRepository repository = connection.getMapper(IUserRepository.class);
            @NotNull final UserDTO user = Optional.ofNullable(repository.findOneById(id))
                    .orElseThrow(UserNotFoundException::new);
            user.setPasswordHash(HashUtil.salt(propertyService, password));
            repository.update(user);
            connection.commit();
            return user;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    public UserDTO updateUser(
            @Nullable final String id,
            @Nullable final String firstName,
            @Nullable final String lastName,
            @Nullable final String middleName
    ) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (firstName == null || firstName.isEmpty()) throw new FirstNameEmptyException();
        if (lastName == null || lastName.isEmpty()) throw new LastNameEmptyException();
        @NotNull final SqlSession connection = connectionService.getConnection();
        try {
            @NotNull final IUserRepository repository = connection.getMapper(IUserRepository.class);
            @NotNull final UserDTO user = Optional.ofNullable(repository.findOneById(id))
                    .orElseThrow(UserNotFoundException::new);
            user.setFirstName(firstName);
            user.setLastName(lastName);
            user.setMiddleName(middleName);
            repository.update(user);
            connection.commit();
            return user;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    public void lockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @NotNull final SqlSession connection = connectionService.getConnection();
        try {
            @NotNull final IUserRepository repository = connection.getMapper(IUserRepository.class);
            @NotNull UserDTO user = Optional.ofNullable(repository.findByLogin(login))
                    .orElseThrow(UserNotFoundException::new);
            user.setLocked(true);
            repository.update(user);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    public void unlockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @NotNull final SqlSession connection = connectionService.getConnection();
        try {
            @NotNull final IUserRepository repository = connection.getMapper(IUserRepository.class);
            @NotNull UserDTO user = Optional.ofNullable(findByLogin(login))
                    .orElseThrow(UserNotFoundException::new);
            user.setLocked(false);
            repository.update(user);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    public boolean isLoginExist(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        try (@NotNull final SqlSession connection = connectionService.getConnection()) {
            @NotNull IUserRepository repository = connection.getMapper(IUserRepository.class);
            return repository.isLoginExist(login);
        }
    }

    @Override
    public boolean isEmailExist(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        try (@NotNull final SqlSession connection = connectionService.getConnection()) {
            @NotNull IUserRepository repository = connection.getMapper(IUserRepository.class);
            return repository.isEmailExist(email);
        }
    }

    @Override
    public void clear() {
        @NotNull final SqlSession connection = connectionService.getConnection();
        try {
            @NotNull final IUserRepository repository =
                    connection.getMapper(IUserRepository.class);
            repository.clear();
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    public boolean existById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        try (@NotNull final SqlSession connection = connectionService.getConnection()) {
            @NotNull final IUserRepository repository =
                    connection.getMapper(IUserRepository.class);
            return repository.existById(id);
        }
    }

    @NotNull
    @Override
    public List<UserDTO> findAll(@Nullable final Sort sort) {
        if (sort == null) return findAll();
        try (@NotNull final SqlSession connection = connectionService.getConnection()) {
            @NotNull final IUserRepository repository =
                    connection.getMapper(IUserRepository.class);
            return repository.findAllSorted(sort.getColumnName());
        }
    }

    @NotNull
    @Override
    public List<UserDTO> findAll() {
        try (@NotNull final SqlSession connection = connectionService.getConnection()) {
            @NotNull final IUserRepository repository =
                    connection.getMapper(IUserRepository.class);
            return repository.findAll();
        }
    }

    @Nullable
    @Override
    public UserDTO findOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        try (@NotNull final SqlSession connection = connectionService.getConnection()) {
            @NotNull final IUserRepository repository =
                    connection.getMapper(IUserRepository.class);
            return repository.findOneById(id);
        }
    }

    @Override
    @NotNull
    public Collection<UserDTO> set(@NotNull Collection<UserDTO> models) {
        @NotNull SqlSession connection = connectionService.getConnection();
        try {
            @NotNull final IUserRepository repository =
                    connection.getMapper(IUserRepository.class);
            repository.clear();
            models.forEach(repository::add);
            connection.commit();
            return models;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

}