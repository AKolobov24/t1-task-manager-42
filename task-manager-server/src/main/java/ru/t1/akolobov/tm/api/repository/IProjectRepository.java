package ru.t1.akolobov.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.akolobov.tm.dto.model.ProjectDTO;

import java.util.List;

public interface IProjectRepository {

    @Update("CREATE TABLE IF NOT EXISTS tm_project " +
            "( " +
            "id character(36) PRIMARY KEY," +
            "created timestamp," +
            "user_id character(36) REFERENCES tm_user (id)," +
            "name varchar(255)," +
            "description text," +
            "status varchar(255)" +
            ");"
    )
    void checkDatabaseTable();

    @Insert("INSERT INTO tm_project " +
            "(id, created, name, description, status, user_id) " +
            "VALUES(#{id}, #{created}, #{name}, #{description}, #{status}, #{userId})"
    )
    void add(@NotNull ProjectDTO model);

    @Update("UPDATE tm_project " +
            "SET name = #{name}, " +
            "description = #{description}, " +
            "status = #{status}, " +
            "user_id = #{userId} " +
            "WHERE id = #{id}"
    )
    void update(@NotNull ProjectDTO model);

    @Delete("DELETE FROM tm_project WHERE user_id = #{userId}")
    void clear(@NotNull String userId);

    @Delete("DELETE FROM tm_project")
    void clearAll();

    @Select("SELECT count(*) > 0 FROM tm_project " +
            "WHERE user_id = #{userId} AND id = #{id} LIMIT 1"
    )
    boolean existById(@NotNull @Param("userId") String userId, @NotNull @Param("id") String id);

    @Select("SELECT * FROM tm_project")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
    })
    @NotNull List<ProjectDTO> findAll();

    @Select("SELECT * FROM tm_project WHERE user_id = #{userId}")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
    })
    @NotNull List<ProjectDTO> findAllByUserId(@NotNull String userId);

    @Select("SELECT * FROM tm_project " +
            "WHERE user_id = #{userId} " +
            "ORDER BY ${sortColumn}")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
    })
    @NotNull List<ProjectDTO> findAllByUserIdSorted(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("sortColumn") String sortColumn
    );

    @Select("SELECT * FROM tm_project " +
            "WHERE user_id = #{userId} AND id = #{id} LIMIT 1")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
    })
    @Nullable ProjectDTO findOneById(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("id") String id
    );

    @Select("SELECT count(*) FROM tm_project WHERE user_id = #{userId}")
    @NotNull Integer getSizeByUserId(@NotNull String userId);

    @Select("SELECT count(*) FROM tm_project")
    @NotNull Integer getSize();

    @Delete("DELETE FROM tm_project WHERE user_id = #{userId} AND id = #{id}")
    void remove(@NotNull ProjectDTO model);

    @Delete("DELETE FROM tm_project WHERE user_id = #{userId} AND id = #{id}")
    void removeById(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("id") String id
    );

}
