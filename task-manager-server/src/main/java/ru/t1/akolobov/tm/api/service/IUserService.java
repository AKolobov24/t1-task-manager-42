package ru.t1.akolobov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.akolobov.tm.dto.model.UserDTO;
import ru.t1.akolobov.tm.enumerated.Role;
import ru.t1.akolobov.tm.enumerated.Sort;

import java.util.Collection;
import java.util.List;

public interface IUserService {

    @NotNull UserDTO add(@Nullable UserDTO user);

    @NotNull
    UserDTO create(@Nullable String login, @Nullable String password);

    @NotNull
    UserDTO create(@Nullable String login, @Nullable String password, @Nullable String email);

    @NotNull
    UserDTO create(@Nullable String login, @Nullable String password, @Nullable Role role);

    @Nullable
    UserDTO findByLogin(@Nullable String login);

    @Nullable
    UserDTO findByEmail(@Nullable String email);

    @NotNull
    UserDTO removeByLogin(@Nullable String login);

    @NotNull
    UserDTO removeByEmail(@Nullable String email);

    @NotNull
    UserDTO remove(@Nullable UserDTO user);

    int removeById(@Nullable String id);

    @NotNull
    UserDTO setPassword(@Nullable String id, @Nullable String password);

    @NotNull
    UserDTO updateUser(@Nullable String id,
                       @Nullable String firstName,
                       @Nullable String lastName,
                       @Nullable String middleName);

    void lockUserByLogin(String login);

    void unlockUserByLogin(String login);

    boolean isLoginExist(String login);

    boolean isEmailExist(String email);

    void clear();

    boolean existById(@Nullable String id);

    @NotNull
    List<UserDTO> findAll(@Nullable Sort sort);

    @NotNull
    List<UserDTO> findAll();

    @Nullable
    UserDTO findOneById(@Nullable String id);

    @NotNull
    Collection<UserDTO> set(@NotNull Collection<UserDTO> models);

}
