package ru.t1.akolobov.tm.service;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.akolobov.tm.api.repository.ISessionRepository;
import ru.t1.akolobov.tm.api.service.IConnectionService;
import ru.t1.akolobov.tm.api.service.ISessionService;
import ru.t1.akolobov.tm.dto.model.SessionDTO;
import ru.t1.akolobov.tm.enumerated.Role;
import ru.t1.akolobov.tm.enumerated.Sort;
import ru.t1.akolobov.tm.exception.entity.EntityNotFoundException;
import ru.t1.akolobov.tm.exception.entity.SessionNotFoundException;
import ru.t1.akolobov.tm.exception.field.DateEmptyException;
import ru.t1.akolobov.tm.exception.field.IdEmptyException;
import ru.t1.akolobov.tm.exception.field.UserIdEmptyException;
import ru.t1.akolobov.tm.exception.user.RoleEmptyException;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Optional;

public class SessionService extends AbstractService implements ISessionService {

    public SessionService(@NotNull IConnectionService connectionService) {
        super(connectionService);
    }

    @NotNull
    @Override
    public SessionDTO add(@Nullable final SessionDTO session) {
        if (session == null) throw new EntityNotFoundException();
        @NotNull SqlSession connection = connectionService.getConnection();
        try {
            @NotNull final ISessionRepository repository =
                    connection.getMapper(ISessionRepository.class);
            repository.add(session);
            connection.commit();
            return session;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    public SessionDTO add(@Nullable final String userId, @Nullable final SessionDTO session) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (session == null) throw new EntityNotFoundException();
        session.setUserId(userId);
        @NotNull SqlSession connection = connectionService.getConnection();
        try {
            @NotNull final ISessionRepository repository =
                    connection.getMapper(ISessionRepository.class);
            repository.add(session);
            connection.commit();
            return session;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    public Collection<SessionDTO> add(@NotNull Collection<SessionDTO> models) {
        @NotNull SqlSession connection = connectionService.getConnection();
        try {
            @NotNull final ISessionRepository repository =
                    connection.getMapper(ISessionRepository.class);
            models.forEach(repository::add);
            connection.commit();
            return models;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    public SessionDTO update(@Nullable final String userId, @Nullable final SessionDTO session) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (session == null) throw new EntityNotFoundException();
        @NotNull SqlSession connection = connectionService.getConnection();
        try {
            @NotNull final ISessionRepository repository =
                    connection.getMapper(ISessionRepository.class);
            if (!repository.existById(userId, session.getId()))
                throw new SessionNotFoundException();
            repository.update(session);
            connection.commit();
            return session;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    public void clear(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final SqlSession connection = connectionService.getConnection();
        try {
            @NotNull final ISessionRepository repository =
                    connection.getMapper(ISessionRepository.class);
            repository.clear(userId);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    public void clear() {
        @NotNull final SqlSession connection = connectionService.getConnection();
        try {
            @NotNull final ISessionRepository repository =
                    connection.getMapper(ISessionRepository.class);
            repository.clearAll();
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    public boolean existById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        try (@NotNull final SqlSession connection = connectionService.getConnection()) {
            @NotNull final ISessionRepository repository =
                    connection.getMapper(ISessionRepository.class);
            return repository.existById(userId, id);
        }
    }

    @NotNull
    @Override
    public List<SessionDTO> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        try (@NotNull final SqlSession connection = connectionService.getConnection()) {
            @NotNull final ISessionRepository repository =
                    connection.getMapper(ISessionRepository.class);
            return repository.findAllByUserId(userId);
        }
    }

    @NotNull
    @Override
    public List<SessionDTO> findAll(final @Nullable String userId, @Nullable final Sort sort) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (sort == null) return findAll(userId);
        try (@NotNull final SqlSession connection = connectionService.getConnection()) {
            @NotNull final ISessionRepository repository =
                    connection.getMapper(ISessionRepository.class);
            return repository.findAllSorted(userId, sort.getColumnName());
        }
    }

    @Nullable
    @Override
    public SessionDTO findOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        try (@NotNull final SqlSession connection = connectionService.getConnection()) {
            @NotNull final ISessionRepository repository =
                    connection.getMapper(ISessionRepository.class);
            return repository.findOneById(userId, id);
        }
    }

    @NotNull
    @Override
    public Integer getSize(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        try (@NotNull final SqlSession connection = connectionService.getConnection()) {
            @NotNull final ISessionRepository repository =
                    connection.getMapper(ISessionRepository.class);
            return repository.getSize(userId);
        }
    }

    @NotNull
    @Override
    public SessionDTO remove(@Nullable final String userId, @Nullable final SessionDTO session) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (session == null) throw new EntityNotFoundException();
        @NotNull final SqlSession connection = connectionService.getConnection();
        try {
            @NotNull final ISessionRepository repository =
                    connection.getMapper(ISessionRepository.class);
            if (!repository.existById(userId, session.getId()))
                throw new SessionNotFoundException();
            repository.remove(session);
            connection.commit();
            return session;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    public void removeById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final SqlSession connection = connectionService.getConnection();
        try {
            @NotNull final ISessionRepository repository =
                    connection.getMapper(ISessionRepository.class);
            repository.removeById(userId, id);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    public SessionDTO updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Date date,
            @Nullable final Role role
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (role == null) throw new RoleEmptyException();
        if (date == null) throw new DateEmptyException();
        @NotNull final SqlSession connection = connectionService.getConnection();
        try {
            @NotNull final ISessionRepository repository =
                    connection.getMapper(ISessionRepository.class);
            @NotNull final SessionDTO session = Optional
                    .ofNullable(repository.findOneById(userId, id))
                    .orElseThrow(SessionNotFoundException::new);
            session.setDate(date);
            session.setRole(role);
            repository.update(session);
            connection.commit();
            return session;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

}
