package ru.t1.akolobov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.akolobov.tm.dto.model.SessionDTO;
import ru.t1.akolobov.tm.enumerated.Role;
import ru.t1.akolobov.tm.enumerated.Sort;

import java.util.Collection;
import java.util.Date;
import java.util.List;

public interface ISessionService {

    @NotNull SessionDTO add(@Nullable SessionDTO session);

    @NotNull SessionDTO add(@Nullable String userId, @Nullable SessionDTO session);

    @NotNull Collection<SessionDTO> add(@NotNull Collection<SessionDTO> models);

    @NotNull SessionDTO update(@Nullable String userId, @Nullable SessionDTO session);

    void clear(@Nullable String userId);

    void clear();

    boolean existById(@Nullable String userId, @Nullable String id);

    @NotNull List<SessionDTO> findAll(@Nullable String userId);

    @NotNull List<SessionDTO> findAll(@Nullable String userId, @Nullable Sort sort);

    @Nullable SessionDTO findOneById(@Nullable String userId, @Nullable String id);

    @NotNull Integer getSize(@Nullable String userId);

    @NotNull SessionDTO remove(@Nullable String userId, @Nullable SessionDTO session);

    void removeById(@Nullable String userId, @Nullable String id);

    @NotNull
    SessionDTO updateById(@Nullable String userId,
                          @Nullable String id,
                          @Nullable Date date,
                          @Nullable Role role);

}
