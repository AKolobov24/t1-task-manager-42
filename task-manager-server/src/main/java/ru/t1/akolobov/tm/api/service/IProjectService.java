package ru.t1.akolobov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.akolobov.tm.dto.model.ProjectDTO;
import ru.t1.akolobov.tm.enumerated.Sort;
import ru.t1.akolobov.tm.enumerated.Status;

import java.util.Collection;
import java.util.List;

public interface IProjectService {

    @NotNull ProjectDTO add(@Nullable String userId, @Nullable ProjectDTO project);

    @NotNull ProjectDTO update(@Nullable String userId, @Nullable ProjectDTO project);

    void clear(@Nullable String userId);

    void clear();

    boolean existById(@Nullable String userId, @Nullable String id);

    @NotNull List<ProjectDTO> findAll();

    @NotNull List<ProjectDTO> findAll(@Nullable String userId);

    @NotNull List<ProjectDTO> findAll(@Nullable String userId, @Nullable Sort sort);

    @Nullable ProjectDTO findOneById(@Nullable String userId, @Nullable String id);

    @NotNull Integer getSize(@Nullable String userId);

    @NotNull ProjectDTO remove(@Nullable String userId, @Nullable ProjectDTO project);

    void removeById(@Nullable String userId, @Nullable String id);

    @NotNull
    ProjectDTO changeStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status);

    @Nullable
    ProjectDTO create(@Nullable String userId, @Nullable String name);

    @Nullable
    ProjectDTO create(@Nullable String userId, @Nullable String name, @Nullable String description);

    @Nullable
    ProjectDTO create(@Nullable String userId, @Nullable String name, @Nullable Status status);

    @NotNull
    ProjectDTO updateById(@Nullable String userId,
                          @Nullable String id,
                          @Nullable String name,
                          @Nullable String description);

    @NotNull Collection<ProjectDTO> add(@NotNull Collection<ProjectDTO> models);

    @NotNull Collection<ProjectDTO> set(@NotNull Collection<ProjectDTO> models);

}
