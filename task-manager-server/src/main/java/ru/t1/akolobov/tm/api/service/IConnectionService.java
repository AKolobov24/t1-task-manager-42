package ru.t1.akolobov.tm.api.service;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.jetbrains.annotations.NotNull;

import javax.persistence.EntityManagerFactory;

public interface IConnectionService {

    @NotNull
    EntityManagerFactory factory();

    @NotNull
    SqlSessionFactory getSqlSessionFactory();

    @NotNull
    SqlSession getConnection();

    @NotNull
    SqlSession getConnection(boolean autoCommit);

}
